package ru.trifonov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.api.ISessionService;
import ru.trifonov.tm.api.IUserService;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.UserDTO;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IUserEndpoint")
public final class UserEndpoint implements IUserEndpoint {
    @NotNull
    ISessionService sessionService;
    @NotNull
    IUserService userService;
    @Autowired
    public UserEndpoint(@NotNull ISessionService sessionService, @NotNull IUserService userService) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "user", partName = "user") @NotNull final User user
    ) throws Exception {
        sessionService.validateAdmin(session);
        userService.persist(user);
    }

    @Override
    @WebMethod
    public void registrationUser(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        userService.registrationUser(login, password);
    }

    @Override
    @WebMethod
    public void registrationAdmin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        sessionService.validateAdmin(session);
        userService.registrationAdmin(login, password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "userId", partName = "userId") @NotNull final String userId,
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        sessionService.validateAdmin(session);
        @NotNull final String id = session.getUserId();
        @NotNull final RoleType roleType = session.getRole();
        userService.update(id, login, password, roleType);
    }

    @Override
    @WebMethod
    public List<UserDTO> getAllUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        @NotNull final List<User> users = userService.getAll();
        @NotNull final List<UserDTO> usersDTO = new ArrayList<>();
        for (@NotNull final User user : users) {
            usersDTO.add(userService.entityToDTO(user));
        }
        return usersDTO;
    }

    @Override
    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String id = session.getUserId();
        @NotNull final User user = userService.get(id);
        return userService.entityToDTO(user);
    }

    @Override
    @WebMethod
    public void deleteUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        sessionService.validateAdmin(session);
        userService.delete(id);
    }

    @Override
    public void changePassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "newPassword", partName = "newPassword") @NotNull String newPassword
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String id = session.getUserId();
        userService.changePassword(id, newPassword);
    }
}