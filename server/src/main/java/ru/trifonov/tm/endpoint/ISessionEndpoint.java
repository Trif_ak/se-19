package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception;
}
