package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    void initTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    void updateTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    TaskDTO getTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    List<TaskDTO> getAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    ) throws Exception;

    @WebMethod
    void deleteTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    void deleteAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    ) throws Exception;

    @WebMethod
    void deleteAllTaskOfUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;

    @WebMethod
    List<TaskDTO> sortByTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull String comparatorName
    ) throws Exception;

    @WebMethod
    List<TaskDTO> getTaskByPartString(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "partString", partName = "partString") @NotNull String partString
    ) throws Exception;

    @WebMethod
    List<TaskDTO> getAllTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;
}
