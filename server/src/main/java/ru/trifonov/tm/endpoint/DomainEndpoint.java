package ru.trifonov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.api.IDomainService;
import ru.trifonov.tm.api.ISessionService;
import ru.trifonov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IDomainEndpoint")
public final class DomainEndpoint implements IDomainEndpoint {
    @NotNull
    ISessionService sessionService;
    @NotNull
    IDomainService domainService;
    @Autowired
    public DomainEndpoint(@NotNull ISessionService sessionService, @NotNull IDomainService domainService) {
        this.sessionService = sessionService;
        this.domainService = domainService;
    }

    @Override
    @WebMethod
    public void domainSerializable(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainSerializable();
    }

    @Override
    @WebMethod
    public void domainDeserializable(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainDeserializable();
    }

    @Override
    @WebMethod
    public void domainSaveJaxbXML(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainSaveJaxbXML();
    }

    @Override
    @WebMethod
    public void domainLoadJaxbXML(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainLoadJaxbXML();
    }

    @Override
    @WebMethod
    public void domainSaveJaxbJSON(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainSaveJaxbJSON();
    }

    @Override
    @WebMethod
    public void domainLoadJaxbJSON(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainLoadJaxbJSON();
    }

    @Override
    @WebMethod
    public void domainSaveJacksonXML(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainSaveJacksonXML();
    }

    @Override
    @WebMethod
    public void domainLoadJacksonXML(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainSaveJacksonXML();
    }

    @Override
    @WebMethod
    public void domainSaveJacksonJSON(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainSaveJacksonJSON();
    }

    @Override
    @WebMethod
    public void domainLoadJacksonJSON(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        domainService.domainLoadJacksonJSON();
    }
}
