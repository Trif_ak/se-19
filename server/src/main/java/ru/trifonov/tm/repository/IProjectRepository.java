package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.entity.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends CrudRepository<Project, String> {
    @Override
    List<Project> findAll();
    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId);
    @Nullable
    Project findOneByIdAndUserId(@NotNull final String id, @NotNull final String userId);
    void deleteAllByUserId(@NotNull final String userId);
    void deleteByIdAndUserId(@NotNull final String id, @NotNull final String userId);
}
