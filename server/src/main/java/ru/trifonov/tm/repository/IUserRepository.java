package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.entity.User;

import java.util.List;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {
    @Override
    List<User> findAll();
    @Nullable
    User findByLoginAndPasswordHash(@NotNull final String login, @NotNull final String passwordHash);
    @Nullable
    User findByLogin(@NotNull final String login);
}
