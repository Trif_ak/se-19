package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.entity.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {
    @Override
    List<Task> findAll();
    @Nullable
    List<Task> findAllByProjectIdAndUserId(@NotNull final String projectId, @NotNull final String userId);
    @Nullable
    Task findOneByIdAndUserId(@NotNull final String id, @NotNull final String userId);
    void deleteAllByUserId(@NotNull final String userId);
    void deleteAllByProjectIdAndUserId(@NotNull final String projectId, @NotNull final String userId);
    void deleteByIdAndUserId(@NotNull final String id, @NotNull final String userId);
}
