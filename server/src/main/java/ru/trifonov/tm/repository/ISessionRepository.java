package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.entity.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends CrudRepository<Session, String> {
    @Override
    List<Session> findAll();
    @Nullable
    List<Session> findAllByUserId(@NotNull final String userId);

}
