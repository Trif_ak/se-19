package ru.trifonov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.api.*;
import ru.trifonov.tm.domain.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

@Service
public final class DomainService extends AbstractService implements IDomainService {
    @NotNull private final IProjectService projectService;
    @NotNull private final ITaskService taskService;
    @NotNull private final IUserService userService;
    @NotNull private final ISessionService sessionService;

    @Autowired
    public DomainService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IUserService userService,
            @NotNull final ISessionService sessionService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    public void export(@Nullable final Domain domain) throws Exception {
        if (domain == null) return;
        domain.setProjects(projectService.getAll());
        domain.setTasks(taskService.getAll());
        domain.setUsers(userService.getAll());
        domain.setSessions(sessionService.getAll());
    }

    @Override
    public void load(@Nullable final Domain domain) throws Exception {
        if (domain == null) return;
        sessionService.load(domain.getSessions());
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void domainSerializable() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try (@NotNull final FileOutputStream domainOut = new FileOutputStream("server/src/main/files/domain");
             @NotNull final ObjectOutputStream domainObjectOut = new ObjectOutputStream(domainOut)
        ) {
            domainObjectOut.writeObject(domain);
        }
    }

    @Override
    public void domainDeserializable() throws Exception {
        try (@NotNull final FileInputStream domainInput = new FileInputStream("server/src/main/files/domain");
             @NotNull final ObjectInputStream domainObjectInput = new ObjectInputStream(domainInput)
        ) {
            load((Domain) domainObjectInput.readObject());
        }
    }

    @Override
    public void domainSaveJaxbXML() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller domainMarshaller = domainContext.createMarshaller();
        domainMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        domainMarshaller.marshal(domain, new File("server/src/main/files/domainJaxb.xml"));
    }

    @Override
    public void domainLoadJaxbXML() throws Exception {
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller domainUnmarshal = domainContext.createUnmarshaller();
        @Nullable final Domain domain = (Domain) domainUnmarshal.unmarshal(new File("server/src/main/files/domainJaxb.xml"));
        if (domain == null) throw new NullPointerException("Something is wrong");
        load(domain);
    }

    @Override
    public void domainSaveJaxbJSON() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller domainMarshaller = domainContext.createMarshaller();
        domainMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        domainMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        domainMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        domainMarshaller.marshal(domain, new File("server/src/main/files/domainJaxb.json"));
    }

    @Override
    public void domainLoadJaxbJSON() throws Exception {
        @NotNull final JAXBContext domainContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller domainUnmarshal = domainContext.createUnmarshaller();
        domainUnmarshal.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        domainUnmarshal.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) domainUnmarshal.unmarshal(new File("server/src/main/files/domainJaxb.xml"));
        load(domain);
    }

    @Override
    public void domainSaveJacksonXML() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try(@NotNull final FileOutputStream domainOut = new FileOutputStream("server/src/main/files/domainJackson.xml")) {
            @NotNull final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
            @NotNull final String domainJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            if (domainJackson == null || domainJackson.trim().isEmpty()) throw new NullPointerException("Something is wrong");
            domainOut.write(domainJackson.getBytes());
            domainOut.flush();
        }
    }

    @Override
    public void domainLoadJacksonXML() throws Exception {
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
        @Nullable final Domain domain = xmlMapper.readerFor(Domain.class).readValue(new File("server/src/main/files/domainJackson.xml"));
        if (domain == null) throw new NullPointerException("Something is wrong");
        load(domain);
    }

    @Override
    public void domainSaveJacksonJSON() throws Exception {
        @NotNull final Domain domain = new Domain();
        export(domain);
        try(@NotNull final FileOutputStream domainOut = new FileOutputStream("server/src/main/files/domainJackson.json")) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String domainJackson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            domainOut.write(domainJackson.getBytes());
            domainOut.flush();
        }
    }

    @Override
    public void domainLoadJacksonJSON() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final Domain domain = objectMapper.readerFor(Domain.class).readValue(new File("server/src/main/files/domainJackson.json"));
        if (domain == null) throw new NullPointerException("Something is wrong");
        load(domain);
    }
}