package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.comparator.DateBeginComparator;
import ru.trifonov.tm.comparator.DateCreateComparator;
import ru.trifonov.tm.comparator.DateEndComparator;
import ru.trifonov.tm.comparator.StatusComparator;
import ru.trifonov.tm.dto.ComparableEntityDTO;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
abstract class ComparatorService extends AbstractService {
    @NotNull private static final Map<String, Comparator<ComparableEntityDTO>> comparators = new LinkedHashMap<>();
    @NotNull private static final Comparator<ComparableEntityDTO> dateBeginComparator = new DateBeginComparator();
    @NotNull private static final Comparator<ComparableEntityDTO> dateEndComparator = new DateEndComparator();
    @NotNull private static final Comparator<ComparableEntityDTO> dateCreateComparator = new DateCreateComparator();
    @NotNull private static final Comparator<ComparableEntityDTO> statusComparator = new StatusComparator();

    static {
        comparators.put("date-create", dateCreateComparator);
        comparators.put("date-begin", dateBeginComparator);
        comparators.put("date-end", dateEndComparator);
        comparators.put("status", statusComparator);
    }

    Comparator<ComparableEntityDTO> getComparator(@NotNull final String comparatorName) {
        return comparators.get(comparatorName);
    }
}
