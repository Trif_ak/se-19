package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

public abstract class AbstractService {
    @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
}
