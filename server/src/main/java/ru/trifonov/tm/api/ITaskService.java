package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.entity.Task;

import java.util.List;

public interface ITaskService {
    void persist(@Nullable Task task);
    void merge(@Nullable Task task);
    void insert(@Nullable String name, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void update(@Nullable String name, @Nullable String id, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    @NotNull Task get(@Nullable String id, @Nullable String userId);
    @NotNull List<Task> getOfProject(@Nullable String projectId, @Nullable String userId);
    void delete(@Nullable String id, @Nullable String userId);
    void deleteOfProject(@Nullable String projectId, @Nullable String userId);
    void deleteOfUser(@Nullable String userId);
    @NotNull List<Task> sortBy(@Nullable String projectId, @Nullable String userId, @Nullable String comparatorName);
    @NotNull List<Task> getByPartString(@NotNull String userId, @NotNull String projectId, @NotNull String partString);
    @NotNull List<Task> getAll();
    void load(@Nullable List<Task> tasks);

    @NotNull TaskDTO entityToDTO(@NotNull Task task);
}
