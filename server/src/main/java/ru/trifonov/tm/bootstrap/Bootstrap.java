package ru.trifonov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.api.IUserService;
import ru.trifonov.tm.endpoint.*;

import javax.xml.ws.Endpoint;

@Component
@PropertySource("classpath:application.properties")
public final class Bootstrap {
    @Value("${server.port}") private String PORT;
    @Value("${server.host}") private String HOST;
    @NotNull
    private final IUserService userService;
    @NotNull
    private final ISessionEndpoint sessionEndpoint;
    @NotNull
    private final IUserEndpoint userEndpoint;
    @NotNull
    private final IProjectEndpoint projectEndpoint;
    @NotNull
    private final ITaskEndpoint taskEndpoint;
    @NotNull
    private final IDomainEndpoint domainEndpoint;
    @Autowired
    public Bootstrap(
            @NotNull final IUserService userService,
            @NotNull final ISessionEndpoint sessionEndpoint,
            @NotNull final IUserEndpoint userEndpoint,
            @NotNull final IProjectEndpoint projectEndpoint,
            @NotNull final ITaskEndpoint taskEndpoint,
            @NotNull final IDomainEndpoint domainEndpoint
    ) {
        this.userService = userService;
        this.sessionEndpoint = sessionEndpoint;
        this.userEndpoint = userEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.taskEndpoint = taskEndpoint;
        this.domainEndpoint = domainEndpoint;
    }

    public void start() {
        System.out.println(PORT + HOST);
        try {
            init();
        } catch (Exception e) {
            System.err.println(e.getMessage());
//            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        initEndpoint();
        initUsers();
    }

    private void initUsers() throws Exception {
        userService.addUser();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + HOST + ":" + PORT + "/" + name + "?wsdl";
        System.out.println(wsdl);
        publisher(wsdl, endpoint);
    }

    private void publisher(@NotNull final String wsdl, @NotNull final Object endpoint) {
        Endpoint.publish(wsdl, endpoint);
    }
}