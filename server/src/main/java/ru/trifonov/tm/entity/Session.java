package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.enumerate.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "sessions")
public final class Session extends AbstractEntity implements Cloneable {
    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @NotNull
    @Column(name = "timestamp", nullable = false)
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    @Column(name = "signature", nullable = false)
    private String signature;

    @NotNull
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleType role = RoleType.REGULAR_USER;

    @NotNull
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

    @NotNull
    @Override
    public String toString() {
        return " ID " + id +
                "  USER_ID " + user.getId() +
                "  SIGNATURE " + signature +
                "  TIMESTAMP " + timestamp +
                "  ROLE " + role;
    }

    public Session(
            @NotNull final String id, @NotNull final User user,
            @NotNull final Long timestamp, @NotNull final String signature, @NotNull final RoleType role
    ) {
        this.id = id;
        this.user = user;
        this.timestamp = timestamp;
        this.signature = signature;
        this.role = role;
    }
}