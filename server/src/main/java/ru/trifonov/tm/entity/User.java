package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.RoleType;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "users")
public final class User extends AbstractEntity {
    @NotNull
    @Column (name = "login", unique = true, nullable = false)
    private String login;

    @NotNull
    @Column (name = "password_hash", nullable = false)
    private String passwordHash;

    @NotNull
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.REGULAR_USER;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<Session> sessions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<Project> projects;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<Task> tasks;

    public User(@NotNull String id, @NotNull String login, @NotNull String passwordHash, @NotNull RoleType roleType) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.roleType = roleType;
        this.id = id;
    }

    public User(@NotNull String login, @NotNull String passwordHash, @NotNull RoleType roleType) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.roleType = roleType;
    }

    @NotNull
    @Override
    public String toString() {
        return " ID " + id +
                "  LOGIN " + login +
                "  PASSWORD_HASH " + passwordHash +
                "  ROLE_TYPE " + roleType;
    }
}