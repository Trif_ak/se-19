package ru.trifonov.tm;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.trifonov.tm.bootstrap.*;
import ru.trifonov.tm.config.Config;

public final class ApplicationServer {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        context.getBean(Bootstrap.class).start();
    }
}