package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "task")
public final class TaskDTO extends ComparableEntityDTO implements Comparable<TaskDTO>, Serializable {
    public TaskDTO(
            @NotNull String projectId, @NotNull String name,
            @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.projectId = projectId;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public TaskDTO(
            @NotNull String id, @NotNull String projectId, @NotNull String userId,
            @NotNull String name, @NotNull String description, @NotNull CurrentStatus status,
            @NotNull Date beginDate, @NotNull Date endDate, @NotNull Date createDate
    ) {
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.status = status;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.createDate = createDate;
    }

    @NotNull
    @Override
    public String toString() {
        return " ID " + id +
                "  PROJECT_ID " + userId +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status +
                "  TASK CREATE DATE " + dateFormat.format(createDate) +
                "  TASK BEGIN DATE " + dateFormat.format(beginDate) +
                "  TASK END DATE " + dateFormat.format(endDate);
    }

    @Override
    public int compareTo(@NotNull TaskDTO task) {
        return this.getId().compareTo(task.getId());
    }
}
