package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "project")
public final class ProjectDTO extends ComparableEntityDTO implements Comparable<ProjectDTO>, Serializable {
    public ProjectDTO(
            @NotNull String userId, @NotNull String name,
            @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public ProjectDTO(
            @NotNull String id, @NotNull String userId, @NotNull String name,
            @NotNull String description, @NotNull CurrentStatus status,  @NotNull Date beginDate,
            @NotNull Date endDate, @NotNull Date createDate
    ) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.status = status;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  USER_ID " + userId +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status +
                "  PROJECT CREATE DATE " + dateFormat.format(createDate) +
                "  PROJECT BEGIN DATE " + dateFormat.format(beginDate) +
                "  PROJECT END DATE " + dateFormat.format(endDate);
    }

    @Override
    public int compareTo(@NotNull ProjectDTO project) {
        return this.getId().compareTo(project.getId());
    }
}