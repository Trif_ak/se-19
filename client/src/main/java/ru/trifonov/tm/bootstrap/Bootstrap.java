package ru.trifonov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;
import java.util.*;

@Setter
@Getter
@Component
public final class Bootstrap {
    @NotNull private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.trifonov.tm.terminal").getSubTypesOf(AbstractCommand.class);
    @NotNull private final TerminalService terminalService;
    @NotNull private final ApplicationContext applicationContext;

    @Autowired
    public Bootstrap(@NotNull TerminalService terminalService, @NotNull ApplicationContext applicationContext) {
        this.terminalService = terminalService;
        this.applicationContext = applicationContext;
    }

    public void start() {
        System.out.println("*** WELCOME TO TASK MANAGER *** \n Enter terminal \"help\" for watch all commands");
        try {
            init(classes);
            while (true) {
                System.out.println("\n Enter command:");
                @NotNull final AbstractCommand abstractCommand =
                        terminalService.getCommands().get(terminalService.getInCommand());
                if (abstractCommand == null) {
                    System.out.println("There is no such command");
                    continue;
                }
                execute(abstractCommand);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void init(@NotNull final Set<Class<? extends AbstractCommand>> commandsClass) {
        for (@NotNull final Class<? extends AbstractCommand> clazz : commandsClass) {
            registry(clazz);
        }
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        @NotNull final AbstractCommand abstractCommand = applicationContext.getBean(clazz);
        @NotNull final String nameCommand = abstractCommand.getName();
        terminalService.getCommands().put(nameCommand, abstractCommand);
    }

    private void execute (@Nullable final AbstractCommand abstractCommand) {
        try {
            abstractCommand.execute();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}


