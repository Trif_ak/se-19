package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class TaskDeleteAllCommand extends AbstractCommand {
    @NotNull final private ITaskEndpoint taskEndpoint;
    @NotNull final private TerminalService terminalService;

    @Autowired
    public TaskDeleteAllCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-deleteAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL TASKS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        taskEndpoint.deleteAllTaskOfProject(currentSession, projectId);
        System.out.println("[OK]");
    }

}
