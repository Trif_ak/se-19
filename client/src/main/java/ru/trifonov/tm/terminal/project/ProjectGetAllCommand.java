package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import java.lang.Exception;
import java.util.Collection;
import java.util.List;

@Component
public final class ProjectGetAllCommand extends AbstractCommand {
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public ProjectGetAllCommand(@NotNull IProjectEndpoint projectEndpoint, @NotNull TerminalService terminalService) {
        this.projectEndpoint = projectEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL PROJECTS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final List<ProjectDTO> inputList = projectEndpoint.getAllProjectOfUser(currentSession);
        for (@NotNull final ProjectDTO project : inputList) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
