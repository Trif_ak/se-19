package ru.trifonov.tm.terminal.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.terminal.AbstractCommand;

@Component
public final class AboutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return information of this application";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT PROGRAM]");
        System.out.println(" Developer: " + Manifests.read("Developer"));
        System.out.println("Version: " + Manifests.read("Version"));
        System.out.println("BuildNumber" + Manifests.read("BuildNumber"));
    }
}
