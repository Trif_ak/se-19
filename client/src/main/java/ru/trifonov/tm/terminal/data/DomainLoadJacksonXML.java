package ru.trifonov.tm.terminal.data;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IDomainEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class DomainLoadJacksonXML extends AbstractCommand {
    @NotNull private final IDomainEndpoint domainEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public DomainLoadJacksonXML(@NotNull IDomainEndpoint domainEndpoint, @NotNull TerminalService terminalService) {
        this.domainEndpoint = domainEndpoint;
        this.terminalService = terminalService;
    }
    @NotNull
    @Override
    public String getName() {
        return "domain-loadJackXML";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": load domain to xml with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN LOAD OF JACKSON XML]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        domainEndpoint.domainLoadJacksonXML(currentSession);
        System.out.println("[OK]");
    }
}
