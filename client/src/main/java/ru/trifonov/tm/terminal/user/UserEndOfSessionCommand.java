package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class UserEndOfSessionCommand extends AbstractCommand {
    @NotNull private final ISessionEndpoint sessionEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserEndOfSessionCommand(@NotNull ISessionEndpoint sessionEndpoint, @NotNull TerminalService terminalService) {
        this.sessionEndpoint = sessionEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": end of user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Log in to log out");
        sessionEndpoint.closeSession(currentSession);
        terminalService.setCurrentSession(null);
        System.out.println("[OK]");
    }
}
