package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class UserRegistrationCommand extends AbstractCommand {
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserRegistrationCommand(@NotNull IUserEndpoint userEndpoint, @NotNull TerminalService terminalService) {
        this.userEndpoint = userEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": registration new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession != null) throw new NullPointerException("LOG OUT to REGISTRATION");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = terminalService.getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = terminalService.getInCommand();
        userEndpoint.registrationUser(login, password);
        System.out.println("[OK]");
    }
}
