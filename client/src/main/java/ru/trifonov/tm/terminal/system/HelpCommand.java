package ru.trifonov.tm.terminal.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class HelpCommand extends AbstractCommand {
    @NotNull private final TerminalService terminalService;

    @Autowired
    public HelpCommand(@NotNull TerminalService terminalService) {
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": show all COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL COMMANDS]");
        for (@NotNull final AbstractCommand command : terminalService.getCommands().values())
            System.out.printf("%-25s > %s \n", command.getName(), command.getDescription());
    }
}
