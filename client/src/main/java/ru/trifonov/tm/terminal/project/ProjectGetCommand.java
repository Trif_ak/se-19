package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;

@Component
public final class ProjectGetCommand extends AbstractCommand {
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public ProjectGetCommand(@NotNull IProjectEndpoint projectEndpoint, @NotNull TerminalService terminalService) {
        this.projectEndpoint = projectEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT GET SELECT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project");
        @Nullable final String id = terminalService.getInCommand();
        @NotNull final ProjectDTO project = projectEndpoint.getProject(currentSession, id);
        System.out.print("  NAME PROJECT " + project.getName());
        System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
        System.out.println("  ID PROJECT " + project.getId());
        System.out.println("[OK]");
    }

}
