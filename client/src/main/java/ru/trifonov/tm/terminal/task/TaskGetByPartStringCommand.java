package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;
import java.util.List;

@Component
public final class TaskGetByPartStringCommand extends AbstractCommand {
    @NotNull final private ITaskEndpoint taskEndpoint;
    @NotNull final private TerminalService terminalService;

    @Autowired
    public TaskGetByPartStringCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.terminalService = terminalService;
    }

    @Override
    public @NotNull String getName() {
        return "task-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        System.out.println("Enter part of title or description");
        @Nullable final String partString = terminalService.getInCommand();
        @NotNull final List<TaskDTO> tasks =
                taskEndpoint.getTaskByPartString(currentSession, projectId, partString);
        for (@NotNull final TaskDTO task : tasks) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }
}
