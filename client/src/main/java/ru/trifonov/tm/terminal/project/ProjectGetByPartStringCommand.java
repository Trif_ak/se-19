package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import java.lang.Exception;
import java.util.List;

@Component
public final class ProjectGetByPartStringCommand extends AbstractCommand {
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public ProjectGetByPartStringCommand(@NotNull IProjectEndpoint projectEndpoint, @NotNull TerminalService terminalService) {
        this.projectEndpoint = projectEndpoint;
        this.terminalService = terminalService;
    }

    @Override
    public @NotNull String getName() {
        return "project-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter part of title or description");
        @Nullable final String partString = terminalService.getInCommand();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.getProjectByPartString(currentSession, partString);
        for (@NotNull final ProjectDTO project : projects) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
