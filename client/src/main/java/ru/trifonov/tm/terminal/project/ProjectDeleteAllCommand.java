package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class ProjectDeleteAllCommand extends AbstractCommand {
    @NotNull private final ITaskEndpoint taskEndpoint;
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public ProjectDeleteAllCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull IProjectEndpoint projectEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-deleteAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL PROJECTS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        taskEndpoint.deleteAllTaskOfUser(currentSession);
        projectEndpoint.deleteAllProject(currentSession);
        System.out.println("[OK]");
    }
}
