package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.Exception_Exception;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class UserAuthorizationCommand extends AbstractCommand {
    @NotNull private final ISessionEndpoint sessionEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserAuthorizationCommand(@NotNull ISessionEndpoint sessionEndpoint, @NotNull TerminalService terminalService) {
        this.sessionEndpoint = sessionEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": authorization in program";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[USER AUTHORIZATION]");
        @Nullable SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession != null) throw new NullPointerException("LOG OUT to LOG IN");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = terminalService.getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = terminalService.getInCommand();
        currentSession = sessionEndpoint.openSession(login, password);
        if (currentSession == null) throw new NullPointerException("Not found user. Please, registration");
        terminalService.setCurrentSession(currentSession);
        System.out.println("[OK]");
    }
}
