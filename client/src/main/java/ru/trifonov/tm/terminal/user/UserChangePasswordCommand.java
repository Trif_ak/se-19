package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class UserChangePasswordCommand extends AbstractCommand {
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserChangePasswordCommand(@NotNull IUserEndpoint userEndpoint, @NotNull TerminalService terminalService) {
        this.userEndpoint = userEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-changePas";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PASSWORD CHANGE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter new password");
        @Nullable final String newPassword = terminalService.getInCommand();
        userEndpoint.changePassword(currentSession, newPassword);
        System.out.println("[OK]");
    }
}