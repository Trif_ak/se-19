package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import java.lang.Exception;
import java.util.Collection;

@Component
public final class TaskGetAllCommand extends AbstractCommand {
    @NotNull final private ITaskEndpoint taskEndpoint;
    @NotNull final private TerminalService terminalService;

    @Autowired
    public TaskGetAllCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": get all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL TASKS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        @NotNull final Collection<TaskDTO> inputList = taskEndpoint.getAllTaskOfProject(currentSession, projectId);
        for (@NotNull final TaskDTO task : inputList) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }
}
