package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import java.lang.Exception;
import java.util.List;

@Component
public class TaskSortCommand extends AbstractCommand {
    @NotNull final private ITaskEndpoint taskEndpoint;
    @NotNull final private TerminalService terminalService;

    @Autowired
    public TaskSortCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.terminalService = terminalService;
    }
    @Override
    public @NotNull String getName() {
        return "task-sort";
    }

    @Override
    public @NotNull String getDescription() {
        return ": sort your tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SORT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        System.out.println("Select the sort type: \n date-create \n date-begin \n date-end \n status");
        @Nullable final String comparatorName = terminalService.getInCommand();
        @NotNull final List<TaskDTO> tasks = taskEndpoint.sortByTask(currentSession, projectId, comparatorName);
        for (@NotNull final TaskDTO task : tasks) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }

}
