package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class TaskInsertCommand extends AbstractCommand {
    @NotNull final private ITaskEndpoint taskEndpoint;
    @NotNull final private TerminalService terminalService;

    @Autowired
    public TaskInsertCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK INSERT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        System.out.println("Enter name task");
        @Nullable final String name = terminalService.getInCommand();
        System.out.println("Enter description");
        @Nullable final String description = terminalService.getInCommand();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        @Nullable final String beginDate = terminalService.getInCommand();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        @Nullable final String endDate = terminalService.getInCommand();
        taskEndpoint.initTask(currentSession, name, projectId, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
