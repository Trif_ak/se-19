package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class UserRemoveOneCommand extends AbstractCommand {
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserRemoveOneCommand(@NotNull IUserEndpoint userEndpoint, @NotNull TerminalService terminalService) {
        this.userEndpoint = userEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE USER]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the user you want to removeOne");
        @Nullable final String id = terminalService.getInCommand();
        userEndpoint.deleteUser(currentSession, id);
        System.out.println("[OK]");
    }
}
