package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class ProjectInsertCommand extends AbstractCommand {
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public ProjectInsertCommand(@NotNull IProjectEndpoint projectEndpoint, @NotNull TerminalService terminalService) {
        this.projectEndpoint = projectEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT INSERT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter name");
        @Nullable final String name = terminalService.getInCommand();
        System.out.println("Enter description");
        @Nullable final String description = terminalService.getInCommand();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = terminalService.getInCommand();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        @Nullable final String endDate = terminalService.getInCommand();
        projectEndpoint.insertProject(currentSession, name, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
