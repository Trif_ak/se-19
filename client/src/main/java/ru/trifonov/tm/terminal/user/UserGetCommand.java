package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;

@Component
public final class UserGetCommand extends AbstractCommand {
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserGetCommand(@NotNull IUserEndpoint userEndpoint, @NotNull TerminalService terminalService) {
        this.userEndpoint = userEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET USER]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final UserDTO userDTO = userEndpoint.getUser(currentSession);
        System.out.print("  LOGIN USER " + userDTO.getLogin());
        System.out.print("  PASSWORD USER " + userDTO.getPasswordHash());
        System.out.println("  ID USER " + userDTO.getId());
        System.out.println("[OK]");
    }
}
