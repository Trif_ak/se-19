package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class UserRegistrationAdminCommand extends AbstractCommand {
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserRegistrationAdminCommand(@NotNull IUserEndpoint userEndpoint, @NotNull TerminalService terminalService) {
        this.userEndpoint = userEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-regAdmin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": registration new admin";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADMIN REGISTRATION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = terminalService.getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = terminalService.getInCommand();
        userEndpoint.registrationAdmin(currentSession, login, password);
        System.out.println("[OK]");
    }
}
