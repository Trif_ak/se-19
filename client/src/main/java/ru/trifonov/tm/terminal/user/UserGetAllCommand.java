package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;
import java.util.Collection;

@Component
public final class UserGetAllCommand extends AbstractCommand {
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public UserGetAllCommand(@NotNull IUserEndpoint userEndpoint, @NotNull TerminalService terminalService) {
        this.userEndpoint = userEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL USERS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final Collection<UserDTO> inputList = userEndpoint.getAllUser(currentSession);
        for (@NotNull final UserDTO userDTO : inputList) {
            System.out.print("  LOGIN USER " + userDTO.getLogin());
            System.out.print("  PASSWORD USER " + userDTO.getPasswordHash());
            System.out.println("  ID USER " + userDTO.getId());
        }
        System.out.println("[OK]");
    }
}
