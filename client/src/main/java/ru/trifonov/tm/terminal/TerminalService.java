package ru.trifonov.tm.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.trifonov.tm.endpoint.SessionDTO;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

@Getter
@Setter
@Service
@NoArgsConstructor
public class TerminalService {
    @NotNull private Scanner inCommand;
    @Nullable private SessionDTO currentSession;

    @Autowired
    public TerminalService(@NotNull final Scanner inCommand) {
        this.inCommand = inCommand;
    }

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public String getInCommand() {
        return inCommand.nextLine();
    }
}
