package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import java.lang.Exception;

@Component
public final class TaskGetCommand extends AbstractCommand {
    @NotNull final private ITaskEndpoint taskEndpoint;
    @NotNull final private TerminalService terminalService;

    @Autowired
    public TaskGetCommand(@NotNull ITaskEndpoint taskEndpoint, @NotNull TerminalService terminalService) {
        this.taskEndpoint = taskEndpoint;
        this.terminalService = terminalService;
    }
    @NotNull
    @Override
    public String getName() {
        return "task-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET TASK]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task");
        @Nullable final String id = terminalService.getInCommand();
        @NotNull final TaskDTO task = taskEndpoint.getTask(currentSession, id);
        System.out.print("  NAME TASK " + task.getName());
        System.out.print("  DESCRIPTION TASK " + task.getDescription());
        System.out.println("  ID TASK " + task.getId());
        System.out.println("[OK]");
    }
}
