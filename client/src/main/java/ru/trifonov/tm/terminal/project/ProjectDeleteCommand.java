package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

@Component
public final class ProjectDeleteCommand extends AbstractCommand {
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final TerminalService terminalService;

    @Autowired
    public ProjectDeleteCommand(@NotNull IProjectEndpoint projectEndpoint, @NotNull TerminalService terminalService) {
        this.projectEndpoint = projectEndpoint;
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT DELETE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project you want delete");
        @Nullable final String id = terminalService.getInCommand();
        projectEndpoint.deleteProject(currentSession, id);
        System.out.println("[OK]");
    }
}
