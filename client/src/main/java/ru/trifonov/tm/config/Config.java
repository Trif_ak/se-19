package ru.trifonov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.trifonov.tm.endpoint.*;

import java.util.Scanner;

@Configuration
@ComponentScan("ru.trifonov.tm")
public class Config {
    @Bean
    @NotNull
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    @NotNull
    public ISessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return new DomainEndpointService().getDomainEndpointPort();
    }
}
